if exists("loaded_tde_indent")
	finish
endif
let loaded_tde_indent = 1

" Fix smartindent
set autoindent                         " Retain indentation on next line
set smartindent                        " Turn on autoindenting of blocks
let g:vim_indent_cont = 0              " No magic shifts on Vim line continuations


" And no shift magic on comments...
nmap <silent>  >>  <Plug>ShiftLine
nnoremap <Plug>ShiftLine :call ShiftLine()<CR>
function! ShiftLine() range
    set nosmartindent
    exec "normal! " . v:count . ">>"
    set smartindent
    silent! call repeat#set( "\<Plug>ShiftLine" )
endfunction
