if exists("loaded_tde_hotreload")
	finish
endif
let loaded_tde_hotreload = 1

" Hot reload when editing vim's config files
augroup VimReload
autocmd!
    autocmd BufWritePost $MYVIMRC source $MYVIMRC
augroup END
