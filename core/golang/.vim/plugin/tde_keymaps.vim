"
" ==== [ KEYMAPS ] ===========================================================
"

if exists("loaded_tde_keymaps")
	finish
endif
let loaded_tde_keymaps = 1

" Global utilities
nnoremap <F1>           :help<CR>

nnoremap <C-t>          :tabnew<CR>
inoremap <C-t>          <Esc>:tabnew<CR>

" Split navigation
nnoremap <C-Left>       <C-w><Left>
nnoremap <C-Right>      <C-w><Right>
nnoremap <C-Up>         <C-w><Up>
nnoremap <C-Down>       <C-w><Down>
inoremap <C-Left>       <ESC><C-w><Left>i
inoremap <C-Right>      <ESC><C-w><Right>i
inoremap <C-Up>         <ESC><C-w><Up>i
inoremap <C-Down>       <ESC><C-w><Down>i

" Save file
nnoremap <C-s>          :w<CR>
inoremap <C-s>          <ESC>:w<CR>i<Right>

" Quit file
nnoremap <C-q>          :q!<CR>
inoremap <C-q>          <ESC>:q!<CR>

" Undo
nnoremap <C-z>          :undo<CR>
vnoremap <C-z>          <ESC>:undo<CR>v
inoremap <C-z>          <ESC>:undo<CR>i

" Redo
nnoremap <C-y>          :redo<CR>
vnoremap <C-y>          <ESC>:redo<CR>v
inoremap <C-y>          <ESC>:redo<CR>i<Right>

" Copy
vnoremap <C-c>          y
inoremap <C-c>          <ESC>yyi<Right>
nnoremap <C-c>          yy

" Cut
vnoremap <C-x>          d
inoremap <C-x>          <ESC>ddi<Right>
nnoremap <C-x>          dd

" Paste
vnoremap <C-v>          p
nnoremap <C-v>          p
inoremap <C-v>          <ESC>pi<Right>

" Select
nnoremap <C-a>          ggVG<CR>
vnoremap <C-a>          <ESC>ggVG<CR>
inoremap <C-a>          <ESC>ggVG<CR>
