if exists("loaded_tde_numbering")
	finish
endif
let loaded_ted_numbering = 1

" Set 'hybrid' line numbering
set number

augroup numbertoggle
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu && mode() != "i" | set rnu     | endif
    autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * if &nu                  | set nornu   | endif
augroup END

