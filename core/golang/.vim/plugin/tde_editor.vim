if exists("loaded_tde_editor")
	finish
endif
let loaded_tde_editor = 1

set nomodeline                         " Avoid modeline vulnerability
set modelines=0                        " Really no modeline

" Prefer vertical orientation when using :diffsplit
set diffopt+=vertical

" Colors
set t_Co=256                           " enable colors in terminal
silent! colorscheme 256_noir
set cursorline

" Match angle brackets..
silent! set matchpairs+=<:>,«:»,｢:｣

" Window
set title                              " Show filename in titlebar of window
set titleold=
"set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:~:.:h\")})%)%(\ %a%)
set title titlestring=

" Removing annoying vim's defaults
" :read and :write shouldn't set #
set cpo-=aA

set mouse=a                            " Enable mouse actions

set nomore                             " Don't page long listings

set cpoptions-=a                       " Don't set # after a :read

set autowrite                          " Save buffer automatically when changing files
set autoread                           " Always reload buffer when external changes detected


set backspace=indent,eol,start         " BS past autoindents, line boundaries,
                                       " and even the start of insertion

set fileformats=unix,mac,dos           " Handle Mac and DOS line-endings
                                       " but prefer Unix endings

set wildignorecase                     " Case-insensitive completions

set wildmode=list:longest,full         " Show list of completions
                                       " and complete as much as possible,
                                       " then iterate full completions

set infercase                          " Adjust completions to match case

set noshowmode                         " Suppress mode change messages

set updatecount=10                     " Save buffer every 10 chars typed


" Keycodes and maps timeout in 3/10 sec...
set timeout timeoutlen=300 ttimeoutlen=300

set updatetime=2000                    " 'idleness' is 2 sec
set scrolloff=2                        " Scroll when 3 lines from top/bottom

set viminfofile=NONE                   " Disable viminfo

set formatoptions-=cro
set formatoptions+=j                  " Remove comment introducers when joining comment lines

set wrapmargin=2                      " Wrap 2 characters from the edge of the window
" set cinwords = ""                   " But not for C-like keywords
set cinoptions+=#1
set cinkeys-=0#
