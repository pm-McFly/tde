if exists("loaded_tde_encoding")
	finish
endif
let loaded_tde_encoding = 1

" Use unicode chars
setglobal termencoding=utf-8 fileencodings=
scriptencoding utf-8
set encoding=utf-8

autocmd BufNewFile,BufRead  *   try
autocmd BufNewFile,BufRead  *       set encoding=utf-8
autocmd BufNewFile,BufRead  *   endtry

