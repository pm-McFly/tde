"
" ==== [ AIRLINE ] ===========================================================
"

if exists("loaded_tde_airline")
	finish
endif
let loaded_tde_airline = 1

let g:airline_theme='hybrid'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
