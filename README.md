```
                                                     ddddddd
                           tttt                      d:::::d
      <<<<<<<           ttt:::t                      d:::::d                          >>>>>>>
     <:::::<            t:::::t                      d:::::d                           >:::::>
    <:::::<             t:::::t                      d:::::d                            >:::::>
   <:::::<        ttttttt:::::ttttttt        ddddddddd:::::d     eeeeeeeeeeee            >:::::>
  <:::::<         t:::::::::::::::::t      dd::::::::::::::d   ee::::::::::::ee           >:::::>
 <:::::<          t:::::::::::::::::t     d::::::::::::::::d  e::::::eeeee:::::ee          >:::::>
<:::::<           tttttt:::::::tttttt    d:::::::ddddd:::::d e::::::e     e:::::e           >:::::>
 <:::::<                t:::::t          d::::::d    d:::::d e:::::::eeeee::::::e          >:::::>
  <:::::<               t:::::t          d:::::d     d:::::d e:::::::::::::::::e          >:::::>
   <:::::<              t:::::t          d:::::d     d:::::d e::::::eeeeeeeeeee          >:::::>
    <:::::<             t:::::t    ttttttd:::::d     d:::::d e:::::::e                  >:::::>
     <:::::<            t::::::tttt:::::td::::::ddddd::::::dde::::::::e                >:::::>
      <<<<<<<           tt::::::::::::::t d:::::::::::::::::d e::::::::eeeeeeee       >>>>>>>
                          tt:::::::::::tt  d:::::::::ddd::::d  ee:::::::::::::e
                            ttttttttttt     ddddddddd   ddddd    eeeeeeeeeeeeee
```

# tde - tiny.development.environments

## Image Versionning

Image versions are based on the [SemVer-2.0.0](https://semver.org/).<br />
Thus the version id follows this pattern:

`<major>.<minor>.<hotfixes> - <stage>.<revision> - <id>`

The version id is so composed of 3 main parts:

  1. **number**
      - `<major>`   : `[0-9]+` - number of mature iterations released.<br />*`0.x.x` reserved for initials development versions.*<br />
      - `<minor>`   : `[0-9]+` - number of small features/fixes added.
      - `<hotfixes>`: `[0-9]+` - number of hotfixes released.
  2. **meta**
      - `<stage>`   : `[stable|beta|alpha]` - indicates wether the image is ready or not ; it level of maturity.<br />*`alpha` and `beta` should only be used for x.0.0 versions.*<br />
      - `<revision>`: `[0-9]+` - number of iterations already done.
  3. **build**
      - `<id>`      : `[0-9a-f]` - id of the commit used to produce the image.

Image version number are stored near the `Containerfile` in a `.version` regular text file.

## Image Tags

To follow the evolution of each image version, you can use these tags:

  - `latest`   : always pointing to the latest image build.
  - `stable`   : pointing to the latest stable version of the image.
  - `<major>`  : pointing to the latest `<major>` version build.
  - `beta`     : pointing to the latest beta version.
  - `alpha`    : pointing to the latest alpha version.
  - `<version>`: pointing to the specific `version` of the image.<br />*kept only 90 days after releasing*

![demo](doc/assets/videos/versionning.webm)

interactive animation: [https://slides.com/pierremarty/tde-image-versionning/](https://slides.com/pierremarty/tde-image-versionning/)

## Image Build

Images are not meant to be build and pushed by a human.<br />
A gitlab runner will perform the build tasks each time a commit is made.

**Only the latest version is build and released to the registry.**
