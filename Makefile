# File		: Makefile
# License	: AGPL-3.0-or-later
# Author	: Pierre Marty <p.mcfly.m@gmail.com>



MAKEFLAGS       +=      -rRs --jobs=$(shell nproc) --output-sync=target
DEPENDS          =      .depends.mk

COLORS		?=	1
ifeq (1, $(COLORS))
	COLOR_RESET	=	\\e[0m
	COLOR_BOLD	=	\\e[1m
	COLOR_RED	=	\\e[31m
	COLOR_GREEN	=	\\e[32m
	COLOR_YELLOW	=	\\e[33m
	COLOR_BLUE	=	\\e[34m
	COLOR_MAGENTA	=	\\e[35m
else
	COLOR_RESET	=
	COLOR_BOLD	=
	COLOR_RED	=
	COLOR_GREEN	=
	COLOR_YELLOW	=
	COLOR_BLUE	=
	COLOR_MAGENTA	=
endif

REGISTRY        ?=      registry.gitlab.com/pm-mcfly/tde

BUILDER          =      docker
BUILDFILE        =      Dockerfile
BUILDFILES       =      $(shell find * -type f -name $(BUILDFILE))
NAMES            =      $(subst /,-,$(subst /$(BUILDFILE),,$(BUILDFILES)))
IMAGES           =      $(addprefix $(subst :,\:,$(REGISTRY))/,$(NAMES))



.PHONY: all clean push pull run help info $(NAMES) $(IMAGES)

all: $(NAMES)

info:
	@echo -e $(COLOR_BOLD) $(COLOR_BLUE)
	@cat .logo.ascii
	@echo -e $(COLOR_GREEN)
	@echo "MAKEFLAGS        += $(MAKEFLAGS)"
	@echo "DEPENDS          += $(DEPENDS)"
	@echo -e $(COLOR_MAGENTA)
	@echo "COLORS           ?= $(COLORS)"
	@echo "---"
	@echo "COLOR_RESET       = $(COLOR_RESET)"
	@echo "COLOR_BOLD        = $(COLOR_BOLD)"
	@echo "COLOR_RED         = $(COLOR_RED)"
	@echo "COLOR_GREEN       = $(COLOR_GREEN)"
	@echo "COLOR_YELLOW      = $(COLOR_YELLOW)"
	@echo "COLOR_BLUE        = $(COLOR_BLUE)"
	@echo "COLOR_MAGENTA     = $(COLOR_MAGENTA)"
	@echo -e $(COLOR_RED)
	@echo "REGISTRY         ?= $(REGISTRY)"
	@echo -e $(COLOR_YELLOW)
	@echo "BUILDER           = $(BUILDER)"
	@echo "BUILDFILE         = $(BUILDFILE)"
	@echo "BUILDFILES        = $(BUILDFILES)"
	@echo "NAMES             = $(NAMES)"
	@echo "IMAGES            = $(IMAGES)"
	@echo -e $(COLOR_RESET)

help:
	@echo -e $(COLOR_BOLD)
	@echo "< tiny.development.environments >"
	@echo "Compilation script"
	@echo ""
	@echo "USAGE: make [\"\"|all|clean|push|pull|run|help] [\"\"|all|<name>]"
	@echo -e $(COLOR_RESET)
	@echo ""
	@echo "Automated Makefile to compute images build order"
	@echo ""
	@echo "make             : build all images"
	@echo "make all         : build all images"
	@echo "make <name>      : build <name> image"
	@echo ""
	@echo "make push all    : push all images"
	@echo "make push <name> : push <name> image"
	@echo ""
	@echo -e "$(COLOR_BOLD)$(COLOR_RED)make run all     : DON'T DO THAT!$(COLOR_RESET)"
	@echo "make run <name>  : build and run <name> image (testing purpose)"
	@echo ""
	@echo "---"
	@echo "By Pierre Marty <p.mcfly.m@gmail.com> a.k.a 'McFly'"
	@echo "for the '< tiny.development.environments >' a.k.a 'tde' project"
	@echo ""
	@echo "sources: https://gitlab.com/pm-McFly/tde/"
	@echo ""
	@echo ""

clean:
	@echo -e $(COLOR_BOLD)
	@echo "====[ REMOVING BUILD ORDER ]===="
	@echo -e $(COLOR_RESET)
	rm -f $(DEPENDS)
	@echo ""

.PHONY: $(DEPENDS)
$(DEPENDS): info $(BUILDFILES)
	@grep '^FROM $(REGISTRY)/' $(BUILDFILES) | \
		awk -F '/$(BUILDFILE):FROM $(REGISTRY)/' '{ print $$1 " " $$2 }' | \
		sed 's@[:/]@\-@g' | \
		awk '{ print "$(subst :,\\:,$(REGISTRY))/" $$1 ": " "$(subst :,\\:,$(REGISTRY))/" $$2 }' > $@

sinclude $(DEPENDS)

$(NAMES): %: $(REGISTRY)/%

$(IMAGES): %:
ifeq (pull,$(filter pull,$(MAKECMDGOALS)))
	@echo -e $(COLOR_BOLD)
	@echo "====[ DOWNLOADING $@ ]===="
	@echo -e $(COLOR_RESET)
	@$(BUILDER) pull $@
else ifeq (push,$(filter push,$(MAKECMDGOALS)))
	@echo -e $(COLOR_BOLD)
	@echo "====[ UPLOADING $@ ]===="
	@echo -e $(COLOR_RESET)
	@for i in $(shell $(BUILDER) image list --format "{{.Tag}}" "$@"); do \
		echo -e $(COLOR_BOLD); \
		echo "====[ SELECTING $$i ]===="; \
		echo -e $(COLOR_RESET); \
		$(BUILDER) push $@:$$i; \
		echo ""; \
	done
else ifeq (run,$(filter run,$(MAKECMDGOALS)))
	@echo -e $(COLOR_BOLD)
	@echo "====[ RUNNING INTERACTIVE $@ ]===="
	@echo -e $(COLOR_RESET)
	@$(BUILDER) run --rm -it $@
else
	$(eval $@_IMG_TAG_VERSION := $(shell cat "$(subst -,/,$(subst $(REGISTRY)/,,$@))/.version"))
	$(eval $@_IMG_TAG_VERSION_MAJ := $(shell echo $($@_IMG_TAG_VERSION) | cut -d . -f 1))
	$(eval $@_IMG_TAG_VERSION_STAGE := $(shell echo $($@_IMG_TAG_VERSION) | cut -d - -f 2 | cut -d . -f 1))
	$(eval $@_IMG_TAG_BUILD := $(shell git log -1 --pretty=%h))
	$(eval $@_IMG_TAG := $($@_IMG_TAG_VERSION)-$($@_IMG_TAG_BUILD))
	$(eval $@_IMG_TAG_LIST := -t $@ -t $@:$($@_IMG_TAG) -t $@:$($@_IMG_TAG_VERSION_STAGE) $(shell if [ $($@_IMG_TAG_VERSION_STAGE) = "stable" ]; then echo "-t $@:$($@_IMG_TAG_VERSION_MAJ)"; fi))

	@echo -e $(COLOR_BOLD)
	@echo "====[ BUILDING $@ ]===="
	@echo -e $(COLOR_RESET)
	@$(BUILDER) build \
		--build-arg REGISTRY=$(REGISTRY) \
		$($@_IMG_TAG_LIST) \
		$(subst -,/,$(subst $(REGISTRY)/,,$@))
endif
