" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes

Plug 'andreasvc/vim-256noir'

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'jistr/vim-nerdtree-tabs', { 'on':  'NERDTreeTabsToggle' }
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" Initialize plugin system
call plug#end()





" dispay a red carret on col 80 if a char is present
augroup collumnLimit
  autocmd!
  autocmd BufEnter,WinEnter,FileType c,cpp
        \ highlight CollumnLimit ctermfg=white ctermbg=red guibg=red guifg=white
  let collumnLimit = 80 " feel free to customize
  let pattern =
        \ '\%<' . (collumnLimit+1) . 'v.\%>' . collumnLimit . 'v'
  autocmd BufEnter,WinEnter,FileType c,cpp
        \ let w:m1=matchadd('CollumnLimit', pattern, -1)
augroup END

" Format using clang-format
if has('python')
    map <C-I> :pyf /usr/share/clang/clang-format.py<cr>
    imap <C-I> <c-o>:pyf /usr/share/clang/clang-format.py<cr>
elseif has('python3')
    map <C-I> :py3f /usr/share/clang/clang-format.py<cr>
    imap <C-I> <c-o>:py3f /usr/share/clang/clang-format.py<cr>
endif
